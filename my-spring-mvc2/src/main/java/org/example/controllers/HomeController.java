package org.example.controllers;

import org.example.view.ProductFeatureService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {
		

	private final Logger logger = LoggerFactory.getLogger(HomeController.class);
	 
	@Autowired
	ProductFeatureService productFeature;
	
    @RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
    public String retrieveProductFeature(@PathVariable String id, Model model) {    	
    	
    	logger.debug("retrieveProductFeature start");
        
    	model.addAttribute("product", productFeature.retrieveProductFeatureById( Integer.valueOf(id)) );
        
        logger.debug("retrieveProductFeature end");
        
        return "index";
    }
}
