package org.example.models;

public enum FeatureDataType {
    STRING,
    NUMERICAL,
    DOUBLE
}
