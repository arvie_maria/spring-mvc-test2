package org.example.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Product entity
 * @author maria.genson
 */
@Entity
@Table(name = "product")
public class Product implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3979245726436434787L;

	public Product() {
	}
	
	public Product(int id, String name, List<Feature> features) {
		super();
		this.id = id;
		this.name = name;
		this.features = features;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "PRODUCT_ID", unique = true, nullable = false)
    private int id;
	
	@Column(name = "PRODUCT_NAME", nullable = false, length = 50)
    private String name;
    
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "feature")
	private List<Feature> features = new ArrayList<Feature>();
	
	public Product(int id, String name) {
        this.id = id;
        this.name = name;
    }
	
    public List<Feature> getFeatures() {
		return features;
	}

	public void setFeatures(List<Feature> features) {
		this.features = features;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
        return id;
    }
	
	public void setName(String name) {
		this.name = name;
	}

    public String getName() {
        return name;
    }

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", options=" + "options]";
	}
}
