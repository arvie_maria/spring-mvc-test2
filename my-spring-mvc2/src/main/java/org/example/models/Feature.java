package org.example.models;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Feature entity
 * @author maria.genson
 */
@Entity
@Table(name = "feature")
public class Feature implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7360622570549266646L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "FEATURE_ID", unique = true, nullable = false)
    private int id;
	
	@Column(name = "FEATURE_NAME", nullable = false, length = 50)
    private String name;
	
    private FeatureDataType dataType;
    
    public Feature(int id, String name, FeatureDataType dataType) {
        this.id = id;
        this.name = name;
        this.dataType = dataType;
    }
    
    public int getId() {
        return id;
    }
    
	public void setId(int id) {
		this.id = id;
	}

    public String getName() {
        return name;
    }
    
	public void setName(String name) {
		this.name = name;
	}

    public FeatureDataType getDataType() {
        return dataType;
    }
	public void setDataType(FeatureDataType dataType) {
		this.dataType = dataType;
	}
    
}
