package org.example.models;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Product-Feature entity
 * @author maria.genson
 */
@Embeddable
public class ProductFeatureId implements java.io.Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "PRODUCT_FEATURE_PRODUCT_ID", nullable = false)
	private int productFeatureProductId;
	
	@Column(name = "PRODUCT_FEATURE_FEATURE_ID", nullable = false)
	private int productFeatureFeatureId;
	
	
	public ProductFeatureId(int productFeatureProductId,
			int productFeatureFeatureId) {
		super();
		this.productFeatureProductId = productFeatureProductId;
		this.productFeatureFeatureId = productFeatureFeatureId;
	}


	public int getProductFeatureProductId() {
		return productFeatureProductId;
	}


	public void setProductFeatureProductId(int productFeatureProductId) {
		this.productFeatureProductId = productFeatureProductId;
	}


	public int getProductFeatureFeatureId() {
		return productFeatureFeatureId;
	}


	public void setProductFeatureFeatureId(int productFeatureFeatureId) {
		this.productFeatureFeatureId = productFeatureFeatureId;
	}
	
	@Override
	public String toString() {
		return "ProductFeatureId [productFeatureProductId=" + productFeatureProductId 
				+ ", productFeatureFeatureId=" + productFeatureFeatureId + "]";
	}
}
