package org.example.models;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "product_feature")
public class ProductFeature implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	@AttributeOverrides({
		@AttributeOverride(name = "productFeatureProductId", column = @Column(name = "PRODUCT_FEATURE_PRODUCT_ID", nullable = false)),
		@AttributeOverride(name = "productFeatureFeatureId", column = @Column(name = "PRODUCT_FEATURE_FEATURE_ID", nullable = false)) })
	private ProductFeatureId id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUCT_FEATURE_FEATURE_ID", nullable = false, insertable = false, updatable = false)
	private Feature feature;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUCT_FEATURE_PRODUCT_ID", nullable = false, insertable = false, updatable = false)
	private Product product;
	
	@Column(name = "DESCRIPTION", nullable = true, length = 100)
	private String description;
	
//	@Column(name = "MEGAPIXELS", nullable = false, length = 20)
//	private Double megapixels;
	
	@Column(name = "IMAGE_URL", nullable = false, length = 100)
	private String imageUrl;
	
	@Column(name = "PRICE", nullable = false, length = 20)
	private Double price;
	
	//[02/11/2016] START : removed megapixels and added generic VALUE column and it's data type
	private String value;
	private FeatureDataType dataType;
	
    public FeatureDataType getDataType() {
        return dataType;
    }
	public void setDataType(FeatureDataType dataType) {
		this.dataType = dataType;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	//[02/11/2016] END
	
	public ProductFeatureId getId() {
		return id;
	}
	public void setId(ProductFeatureId id) {
		this.id = id;
	}
	public Feature getFeature() {
		return feature;
	}
	public void setFeature(Feature feature) {
		this.feature = feature;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
//	public Double getMegapixels() {
//		return megapixels;
//	}
//	public void setMegapixels(Double megapixels) {
//		this.megapixels = megapixels;
//	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "ProductFeature [id=" + id + ", feature=" + feature
				+ ", product=" + product + ", description=" + description
				+ ", value=" + value + ", imageUrl=" + imageUrl
				+ ", price=" + price + ", dataType=" + dataType + "]";
	}
	
	
}
