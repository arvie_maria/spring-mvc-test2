package org.example.view;


import org.example.dao.ProductFeatureDao;
import org.example.models.FeatureDataType;
import org.example.models.Product;
import org.example.models.ProductFeature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductFeatureServiceImpl implements ProductFeatureService {

	@Autowired
	ProductFeatureDao productFeatureDao;
	
	@Override
	public ProductFeature retrieveProductFeatureById(int id) {
		
		ProductFeature featureResponse = new ProductFeature();
		
		featureResponse.setDescription("SAMPLE DESCRIPTION");
		featureResponse.setImageUrl("<IMAGE/URL>");
		featureResponse.setValue("megapixels");
		featureResponse.setDataType(FeatureDataType.DOUBLE);
		
		Product product = new Product();
		product.setName("Camera");
		featureResponse.setProduct(product);
		
		return featureResponse;
	}

}
