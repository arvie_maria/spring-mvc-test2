package org.example.view;

import org.example.models.ProductFeature;

public interface ProductFeatureService {

	// Class use to retrieve Features of the Product
	public ProductFeature retrieveProductFeatureById(int id);
}
